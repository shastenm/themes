MarkSparrow it's a dark and simple xfwm theme with many colour variations.

It's based on the xfwm4 of the Rich Colors theme which is based on the Matcha Gtk Theme.
https://www.pling.com/p/1955537/
https://www.pling.com/p/1187179/
https://github.com/vinceliuice/Matcha-gtk-theme

This is the xfwm theme I use on the full themes I edit and upload here on pling. I think it also fits many other themes. For a better result I suggest to combine it with medium or bold fonts with 9-10 font size. The font used on the preview it's Ubuntu Medium with font size 10. And the icons is Delft Green.


